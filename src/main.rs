use core::marker::PhantomData;
use num_traits::Num;
use rug::ops::Pow;
use rug::Assign;
use rug::Float;

struct Generator<F, Fun>
where
    F: Sized,
{
    _tp: PhantomData<F>,
    _fun: PhantomData<Fun>,
}

mod configs;
mod solve;
use softposit::P32;

const PREC: u32 = 4096;

const Q: usize = 10000;

pub trait PrecTarget {
    const PREC_TARGET: u32;
}

pub trait ToF64 {
    fn to_f64(self) -> f64;
}

pub trait TargetFun {
    fn target(ret: &mut Float, a: &Float);
    /// The function to approximate
    fn cfunc(dst: &mut Float, src: &Float);
}

pub trait Config<F>
where
    F: Sized + Num,
{
    /// Degree of equation
    const N: usize;
    /// Number of samples for phase 1
    const S: usize;
    /// Number of high precision coefficients
    const L: usize;
    const P: Option<F>;
    /// Min argument
    const MIN: F;
    /// Max argument
    const MAX: F;
    /// The form of polynomial is y = x^(PADD+PMUL*0) + x^(PADD+PMUL*1) + ...
    const PMUL: F;
    const PADD: F;
    const FIXCOEF0: Option<F>;
    const FIXCOEF1: Option<F>;
    const FIXCOEF2: Option<F>;
}

impl<F, Fun> Generator<F, Fun>
where
    Self: Config<F>,
    Fun: TargetFun,
    F: num_traits::One + num_traits::Num + PrecTarget + ToF64,
{
    fn count_ulp(d: &Float, c: &Float) -> f64 {
        let c2 = c.to_f64();
        if (c2 == 0.) && (*d != 0_f64) {
            return 10000.;
        }

        let prec = d.prec().max(c.prec());

        let frw = Float::new(prec);
        let mut fry = Float::new(prec);

        let (_, e) = c.to_f64_exp();
        use gmp_mpfr_sys::mpfr;
        unsafe {
            mpfr::set_ui_2exp(
                &frw as *const Float as *mut mpfr::mpfr_t,
                1,
                (e - (F::PREC_TARGET as i32)) as i64,
                mpfr::rnd_t::RNDN,
            );
        }

        fry.assign(d - c);
        fry /= &frw;
        let u = fry.to_f64().abs();

        u
    }

    /*fn func(s: &mut Float, x: &Float, coef: &[Float]) {
        let prec2 = F::PREC_TARGET * 4;
        let n = coef.len();
        s.set_prec(F::PREC_TARGET);
        s.assign(&coef[n - 1]);

        for i in (0..n).rev() {
            if i == Self::L - 1 {
                let mut t = Float::new(prec2);
                t.assign(&s.clone());
                s.set_prec(prec2);
                s.assign(&t);
                // mpfr::clear(&mut t);
            }
            s.mul_assign(x);
            s.add_assign(&coef[i - 1]);
        }
    }*/

    fn run() {
        let prec2 = F::PREC_TARGET * 4;
        use rand::Rng;
        use std::env;
        let mut rng = rand::thread_rng();
        //       mpfr::set_default_prec(PREC);

        #[cfg(all(feature = "to100"))]
        {
            let b = Float::new(PREC);
            let x = F::pi();
            let a = Float::with_val(x);
            let F100 = Float::with_val(100., PREC);
            x = x.next_toward(F100);
            x = x.next_toward(F100);
            x = x.next_toward(F100);
            b.assign(&x);

            println!("{}", count_ulp(&b, &a));
            exit(0);
        }
        /*
        #if 0
            {
                mpfr_t a, b;
                mpfr::inits(a, b, NULL);

                __float128 x = M_PI;
                mpfr::set_f128(&mut a, &x, Round::Nearest);
                x = nextafterq(x, 100.);
                x = nextafterq(x, 100.);
                x = nextafterq(x, 100.);
                mpfr::set_f128(&mut b, &x, Round::Nearest);

                println!("{}", count_ulp(&b, &a));
                exit(0);
            }
        #endif
        */

        let m = Self::N + 1;
        let n: usize = if let Some(arg1) = env::args().nth(1) {
            arg1.parse().unwrap()
        } else {
            Self::S
        };
        let p: f64 = if let Some(arg2) = env::args().nth(2) {
            arg2.parse().unwrap()
        } else {
            Self::P.unwrap_or(F::one()).to_f64()
        };

        let mut x = vec![vec![Float::new(PREC); n]; m];

        let mut fra = Float::new(PREC);
        let mut frb = Float::new(PREC);
        let mut frc = Float::new(PREC);
        let mut frd = Float::new(PREC);
        //let mut fre = Float::new(PREC);

        let mx = Self::MAX.to_f64();
        let mn = Self::MIN.to_f64();
        for i in 0..n {
            let b = 1. - ((i as f64) / ((n - 1) as f64)).powf(p);
            let a = (mx - mn) * b + mn;
            fra.assign(a);
            Fun::cfunc(&mut frd, &fra);

            for j in 0..m - 1 {
                frb.assign((j as f64) * Self::PMUL.to_f64() + Self::PADD.to_f64());
                x[j][i].assign(frd.clone().pow(&frb));
                //print!("{} ", x[j][i].to_f64());
            }

            Fun::target(&mut x[m - 1][i], &fra);
            //println!(" : {}", x[m-1][i].to_f64());
        }

        let result = solve::regress_min_rel_error_fr(PREC, n, m - 1, &x);

        for i in (0..m - 1).rev() {
            fra.set_prec(F::PREC_TARGET + 4);
            fra.assign(&result[i]);

            println!("{}, ", fra);
        }
        println!();

        fra.set_prec(PREC);

        let mut emax = 0_f64;

        for i in 0..=n * 10 {
            let a: f64 = (i as f64) * (mx - mn) / ((n as f64) * 10.) + mn;
            fra.assign(a);

            Fun::cfunc(&mut frd, &fra);

            &mut frb.assign(0.);

            for j in (0..m).rev() {
                frc.assign((j as f64) * Self::PMUL.to_f64() + Self::PADD.to_f64());
                frc.assign(frd.clone().pow(&frc));
                frc *= &result[j];
                frb += &frc;
            }

            Fun::target(&mut frc, &fra);
            let u: f64 = Self::count_ulp(&frb, &frc);

            if u > emax {
                emax = u;
            }
        }

        println!("Phase 1 : Max error = {} ULP\n", emax);

        let mut bestcoef = vec![Float::new(PREC); Self::N];
        let mut curcoef = vec![Float::new(PREC); Self::N];

        for i in 0..Self::N {
            bestcoef[i] = Float::new(if i >= Self::L { F::PREC_TARGET } else { prec2 });
            bestcoef[i].assign(&result[i]);

            curcoef[i] = Float::new(if i >= Self::L { F::PREC_TARGET } else { prec2 });
            curcoef[i].assign(&result[i]);
        }

        let mut a = vec![Float::new(prec2); Q];
        let mut v = vec![Float::new(prec2); Q];
        let mut am = vec![Float::new(prec2); Q];
        let mut aa = vec![Float::new(prec2); Q];

        for i in 0..Q {
            fra.assign(
                (Self::MAX.to_f64() - Self::MIN.to_f64()) * (i as f64) / ((Q - 1) as f64)
                    + Self::MIN.to_f64(),
            );

            Fun::target(&mut v[i], &fra);
            Fun::cfunc(&mut a[i], &fra);
            let ai = &a[i].clone();
            frb.assign(Self::PMUL.to_f64());
            am[i].assign(ai.pow(&frb));
            frb.assign(Self::PADD.to_f64());
            aa[i].assign(ai.pow(&frb));
        }

        let mut best = 1e+100;
        let mut bestsum = 1e+100;
        let mut bestworstx = 0_f64;

        let mut pb = pbr::ProgressBar::new(10000);
        let mut k = 0;
        while k < 10000 {
            let mut emax = 0_f64;
            let mut esum = 0_f64;
            let mut worstx = 0_f64;

            if let Some(fix) = Self::FIXCOEF0 {
                curcoef[0].assign(fix.to_f64());
            }

            if let Some(fix) = Self::FIXCOEF1 {
                curcoef[1].assign(fix.to_f64());
            }

            if let Some(fix) = Self::FIXCOEF2 {
                curcoef[2].assign(fix.to_f64());
            }

            for i in 0..Q {
                if v[i] == 0. {
                    continue;
                }

                frb.assign(0.);
                for j in (0..Self::N).rev() {
                    frc.assign((j as f64) * Self::PMUL.to_f64() + Self::PADD.to_f64());
                    frc.assign(a[i].clone().pow(&frc));
                    frc *= &curcoef[j];
                    frb += &frc;
                }

                let e = Self::count_ulp(&frb, &v[i]);

                //println!("c = {:.20}, t = {:.20}, ulp = {}", v[i].to_f64(), frb.to_f64(), e);

                if !e.is_finite() {
                    continue;
                }
                if e > emax {
                    emax = e;
                    worstx = a[i].to_f64();
                }
                esum += e;
            }
            frb.set_prec(PREC);

            //println!("emax = {}", emax);

            if (emax < best) || ((emax == best) && (esum < bestsum)) {
                for i in 0..Self::N {
                    bestcoef[i].assign(&curcoef[i]);
                }
                if (best == 1e+100) || (k > 10) {
                    println!(
                        "Max error = {} ULP, Sum error = {} (Max error at {})",
                        emax, esum, worstx
                    );
                }
                if (best - emax) / best > 0.0001 {
                    k = 0;
                    pb.set(0);
                }
                best = emax;
                bestsum = esum;
                bestworstx = worstx;
            }

            for i in 0..Self::N {
                curcoef[i].assign(&bestcoef[i]);
            }

            for i in 0..Self::N {
                const TAB: [isize; 8] = [0, 0, 0, 0, 0, 0, 1, -1];
                //const TAB: [isize; 8] = [0, 0, 0, 0, 2, -2, 1, -1];
                let r = TAB[rng.gen::<usize>() & 7];
                if r > 0 {
                    for _j in 0..r {
                        curcoef[i].next_up();
                    }
                } else if r < 0 {
                    for _j in (r + 1..=0).rev() {
                        curcoef[i].next_down();
                    }
                }
            }
            k += 1;
            pb.inc();
        }
        pb.finish_print("done");

        println!();

        for i in (0..Self::N).rev() {
            fra.set_prec(if i >= Self::L {
                F::PREC_TARGET + 4
            } else {
                prec2
            });
            fra.assign(&bestcoef[i]);

            println!("{}, ", fra);
        }
        println!("\nPhase 2 : max error = {} ULP at {}", best, bestworstx);
    }
}

use core::ops::{DivAssign, SubAssign};
use rug::float::Constant;

pub struct Sin;
impl TargetFun for Sin {
    fn target(ret: &mut Float, a: &Float) {
        ret.assign(a.sin_ref());
    }
    fn cfunc(dst: &mut Float, src: &Float) {
        dst.assign(src);
    }
}

pub struct Cos;
impl TargetFun for Cos {
    fn target(ret: &mut Float, a: &Float) {
        // cos(x) - 1
        ret.assign(a.cos_ref());
        let x = Float::with_val(ret.prec(), 1.);
        ret.sub_assign(&x);
    }
    fn cfunc(dst: &mut Float, src: &Float) {
        dst.assign(src);
    }
}

pub struct SinPi4;
impl TargetFun for SinPi4 {
    fn target(ret: &mut Float, a: &Float) {
        let mut x = Float::with_val(ret.prec(), Constant::Pi);
        let y = Float::with_val(ret.prec(), 1. / 4.);
        x *= &y;
        x *= a;
        ret.assign(&x.sin());
    }
    fn cfunc(dst: &mut Float, src: &Float) {
        dst.assign(src);
    }
}

pub struct CosPi4;
impl TargetFun for CosPi4 {
    fn target(ret: &mut Float, a: &Float) {
        let mut x = Float::with_val(ret.prec(), Constant::Pi);
        let y = Float::with_val(ret.prec(), 1. / 4.);
        x *= &y;
        x *= a;
        ret.assign(x.cos_ref());
        x.assign(1.);
        ret.sub_assign(&x);
    }
    fn cfunc(dst: &mut Float, src: &Float) {
        dst.assign(src);
    }
}

pub struct Tan;
impl TargetFun for Tan {
    fn target(ret: &mut Float, a: &Float) {
        ret.assign(a.tan_ref());
    }
    fn cfunc(dst: &mut Float, src: &Float) {
        dst.assign(src);
    }
}

pub struct Ln;
impl TargetFun for Ln {
    fn target(ret: &mut Float, a: &Float) {
        ret.assign(a.ln_ref());
    }
    fn cfunc(frd: &mut Float, fra: &Float) {
        let mut tmp = Float::new(frd.prec());
        let one = Float::with_val(frd.prec(), 1.);
        tmp.assign(fra + &one);
        frd.assign(fra - &one);
        frd.div_assign(&tmp);
    }
}

pub struct Exp;
impl TargetFun for Exp {
    fn target(ret: &mut Float, a: &Float) {
        ret.assign(a.exp_ref());
    }
    fn cfunc(dst: &mut Float, src: &Float) {
        dst.assign(src);
    }
}

pub struct Atan;
impl TargetFun for Atan {
    fn target(ret: &mut Float, a: &Float) {
        ret.assign(a.atan_ref());
    }
    fn cfunc(dst: &mut Float, src: &Float) {
        dst.assign(src);
    }
}

pub struct Asin;
impl TargetFun for Asin {
    fn target(ret: &mut Float, a: &Float) {
        ret.assign(a.asin_ref());
    }
    fn cfunc(dst: &mut Float, src: &Float) {
        dst.assign(src);
    }
}

fn main() {
    Generator::<f32, Cos>::run();
}
