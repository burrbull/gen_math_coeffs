use core::cmp::Ordering;
use rug::Float;
use rug::{ops::NegAssign, Assign};

const EPS: f64 = 1e-50;

enum SolveErr {
    MaximizableToInf,
    NotFeasible,
}

struct Solver {
    prec: u32,

    m: usize, // number of inequations
    n: usize, // number of variables
    n1: usize,
    n2: usize,
    n3: usize,
    jmax: usize,

    col: Vec<usize>,
    row: Vec<usize>,

    nonzero_row: Vec<usize>,
    inequality: Vec<Option<Ordering>>,

    a: Vec<Vec<Float>>,
    c: Vec<Float>,
    q: Vec<Vec<Float>>,
    pivotcolumn: Vec<Float>,

    // consts
    one: Float,
    eps: Float,
    minuseps: Float,
    large: Float,
}

impl Solver {
    fn init(prec: u32, epsilon: f64, n0: usize, m0: usize) -> Self {
        let n = n0;
        let m = m0;
        Solver {
            prec,

            n,
            m,

            one: Float::with_val(prec, 1.),
            eps: Float::with_val(prec, epsilon),
            minuseps: Float::with_val(prec, -epsilon),
            large: Float::with_val(prec, 1. / epsilon),

            a: vec![vec![Float::new(prec); n + 1]; m + 1],
            q: vec![vec![Float::new(prec); m + 1]; m + 1],
            c: vec![Float::new(prec); n + 1],
            pivotcolumn: vec![Float::new(prec); m + 1],

            col: vec![0; m + 1],
            row: vec![0; n + 2 * m + 1],
            nonzero_row: vec![0; n + 2 * m + 1],
            inequality: vec![None; m + 1],

            n1: 0,
            n2: 0,
            n3: 0,
            jmax: 0,
        }
    }
}

impl Solver {
    fn prepare(&mut self) {
        self.n1 = self.n;
        let m = self.m;
        for i in 1..=m {
            if self.inequality[i] == Some(Ordering::Greater) {
                self.n1 += 1;
                self.nonzero_row[self.n1] = i;
            }
        }
        self.n2 = self.n1;
        for i in 1..=m {
            if self.inequality[i] == Some(Ordering::Less) {
                self.n2 += 1;
                self.col[i] = self.n2;
                self.nonzero_row[self.n2] = i;
                self.row[self.n2] = i;
            }
        }
        self.n3 = self.n2;
        for i in 1..=m {
            if self.inequality[i] != Some(Ordering::Less) {
                self.n3 += 1;
                self.col[i] = self.n3;
                self.nonzero_row[self.n3] = i;
                self.row[self.n3] = i;
            }
        }

        for i in 0..=m {
            self.q[i][i].assign(1.);
        }
    }

    fn tableau(&self, ret: &mut Float, i: usize, j: usize) {
        let m = self.m;
        //if self.col[i] < 0 {
        if self.col[i] == (-1_isize) as usize {
            ret.assign(0.);
            return;
        }

        if j <= self.n {
            let mut s = Float::new(self.prec);

            let mut tab = vec![Float::new(self.prec); m + 1];

            for k in 0..=m {
                tab[k].assign(&self.q[i][k] * &self.a[k][j]);
            }
            s.assign(Float::sum(tab.iter()));
            ret.assign(s);
            return;
        }

        ret.assign(&self.q[i][self.nonzero_row[j]]);

        if j <= self.n1 {
            ret.neg_assign();
            return;
        }
        if (j <= self.n2) || (i != 0) {
            return;
        }

        *ret += &self.one;
        return;
    }

    fn pivot(&mut self, ipivot: usize, jpivot: usize) {
        let m = self.m;
        let mut u = Float::new(self.prec);

        u.assign(&self.pivotcolumn[ipivot]);

        for j in 1..=m {
            self.q[ipivot][j] /= &u;
        }

        for i in 0..=m {
            if i != ipivot {
                u.assign(&self.pivotcolumn[i]);

                for j in 1..=m {
                    let f1 = self.q[ipivot][j].clone();
                    let f2 = self.q[i][j].clone();
                    self.q[i][j] = f1.mul_sub(&u, &f2);
                    self.q[i][j].neg_assign();
                }
            }
        }

        self.row[self.col[ipivot]] = 0;

        self.col[ipivot] = jpivot;
        self.row[jpivot] = ipivot;
    }

    fn minimize(&mut self) -> Result<(), SolveErr> {
        let mut t = Float::new(self.prec);
        let mut u = Float::new(self.prec);

        loop {
            let mut jpivot = 1;
            while jpivot <= self.jmax {
                if self.row[jpivot] == 0 {
                    let mut f = self.pivotcolumn[0].clone();
                    self.tableau(&mut f, 0, jpivot);
                    self.pivotcolumn[0] = f;
                    if self.pivotcolumn[0] < self.minuseps {
                        break;
                    }
                }
                jpivot += 1;
            }
            if jpivot > self.jmax {
                return Ok(());
            }

            u.assign(&self.large);
            let mut ipivot = 0;
            for i in 1..=self.m {
                let mut f = self.pivotcolumn[i].clone();
                self.tableau(&mut f, i, jpivot);
                self.pivotcolumn[i] = f;
                if self.pivotcolumn[i] > self.eps {
                    self.tableau(&mut t, i, 0);
                    t /= &self.pivotcolumn[i];
                    if t < u {
                        ipivot = i;
                        u.assign(&t);
                    }
                }
            }

            if ipivot == 0 {
                return Err(SolveErr::MaximizableToInf); // the objective function can be minimized to -infinite
            }
            self.pivot(ipivot, jpivot);
        }
    }

    fn phase1(&mut self) -> Result<(), SolveErr> {
        let m = self.m;
        let mut u = Float::new(self.prec);

        self.jmax = self.n3;
        for i in 0..=m {
            if self.col[i] > self.n2 {
                self.q[0][i].assign(-1.);
            }
        }

        self.minimize()?;

        self.tableau(&mut u, 0, 0);
        if u < self.minuseps {
            return Err(SolveErr::NotFeasible);
        }
        for i in 1..=m {
            if self.col[i] > self.n2 {
                self.col[i] = (-1_isize) as usize;
            }
        }
        self.q[0][0].assign(1.);
        for j in 1..=m {
            self.q[0][j].assign(0.);
        }
        for i in 1..=m {
            let j = self.col[i];
            if (j > 0) && (j <= self.n) && (self.c[j] != 0_f64) {
                u.assign(&self.c[j]);
                for j in 1..=m {
                    let f1 = self.q[i][j].clone();
                    let f2 = self.q[0][j].clone();
                    self.q[0][j].assign(f1.mul_sub(&u, &f2));
                    self.q[0][j].neg_assign();
                }
            }
        }
        Ok(())
    }

    fn phase2(&mut self) -> Result<(), SolveErr> {
        self.jmax = self.n2;
        for j in 0..=self.n {
            self.a[0][j].assign(&self.c[j]);
        }

        self.minimize()
    }

    fn solve_fr(
        &mut self,
        n0: usize,
        a0: &Vec<Vec<Float>>,
        ineq0: Vec<Option<Ordering>>,
        c0: &Vec<Float>,
    ) -> Result<Vec<Float>, SolveErr> {
        let n = self.n;
        let mut csum = Float::new(self.prec);

        for j in 0..=n0 {
            self.c[j].assign(&c0[j]);
        }

        for j in 1..=n0 {
            csum += &c0[j];
        }

        self.c[n].assign(&csum);
        self.c[n].neg_assign();

        for i in 0..self.m {
            csum.assign(0.);

            for j in 0..=n0 {
                self.a[i + 1][j].assign(&a0[i][j]);
            }
            self.a[i + 1][0].neg_assign();

            for j in 1..=n0 {
                csum += &a0[i][j];
            }

            self.a[i + 1][n].assign(&csum);
            self.a[i + 1][n].neg_assign();
            self.inequality[i + 1] = ineq0[i];
            if self.a[i + 1][0] < 0_f64 {
                if self.inequality[i + 1] == Some(Ordering::Greater) {
                    self.inequality[i + 1] = Some(Ordering::Less);
                } else if self.inequality[i + 1] == Some(Ordering::Less) {
                    self.inequality[i + 1] = Some(Ordering::Greater);
                }
                for j in 0..=n {
                    self.a[i + 1][j].neg_assign();
                }
            } else if (self.a[i + 1][0] == 0.)
                && (self.inequality[i + 1] == Some(Ordering::Greater))
            {
                self.inequality[i + 1] = Some(Ordering::Less);
                for j in 1..=n {
                    self.a[i + 1][j].neg_assign();
                }
            }
        }

        self.prepare();

        if self.n3 != self.n2 {
            self.phase1()?;
        }

        self.phase2()?;

        let mut s = vec![Float::new(self.prec); n];

        for j in 1..n {
            let i = self.row[j];
            if i != 0 {
                self.tableau(&mut s[j], i, 0);
            }
        }

        let mut cs = Float::new(self.prec);

        if self.row[n] != 0 {
            self.tableau(&mut cs, self.row[n], 0);
        }

        for j in 1..n {
            s[j] -= &cs;
        }

        let mut result0 = vec![Float::new(self.prec); n0 + 1];
        for j in 0..n {
            result0[j].assign(&s[j]);
        }
        Ok(result0)
    }
}

pub fn regress_min_rel_error_fr(prec: u32, n: usize, m: usize, x: &Vec<Vec<Float>>) -> Vec<Float> {
    let m0 = n * 3;
    let n0 = m + 2 * n;
    let mut in0 = vec![None; m0];

    let mut a0 = vec![vec![Float::new(prec); n0 + 1]; m0];
    let mut c0 = vec![Float::new(prec); n0 + 1];

    for i in 0..n {
        //
        let mut ld = x[m][i].clone();
        if ld < core::f64::MIN_POSITIVE {
            ld = Float::with_val(prec, 1.);
        }
        let lr = ld.abs().recip();
        c0[m + i + 1].assign(&lr);
        c0[m + n + i + 1].assign(&lr);
        //
        /*    long double ld = mpfr::get_ld(x[m][i], Round::Nearest);
            if (ld < f64::MIN_POSITIVE) ld = 1;

        #if 1
            c0[m+i  +1].assign(1./fabsl(ld));
            c0[m+n+i+1].assign(1./fabsl(ld));
        #else
            int e;
            frexpl(ld, &e);
            ld = 1. / ldexpl(1., e);
            c0[m+i  +1].assign(ld);
            c0[m+n+i+1].assign(ld);
        #endif
            */
        a0[i * 3 + 0][m + i + 1].assign(1.);
        in0[i * 3 + 0] = Some(Ordering::Greater);

        a0[i * 3 + 1][m + n + i + 1].assign(1.);
        in0[i * 3 + 1] = Some(Ordering::Greater);

        for j in 0..m {
            a0[i * 3 + 2][j + 1].assign(&x[j][i]);
        }

        a0[i * 3 + 2][m + i + 1].assign(1.);
        a0[i * 3 + 2][m + n + i + 1].assign(-1);
        in0[i * 3 + 2] = Some(Ordering::Equal);
        a0[i * 3 + 2][0].assign(&x[m][i]);
        a0[i * 3 + 2][0].neg_assign();
    }

    let status = {
        let mut solver = Solver::init(prec, EPS, n0 + 1, m0);
        solver.solve_fr(n0, &a0, in0, &c0)
    };

    match status {
        Ok(result0) => {
            let mut result = vec![Float::new(prec); m + 1];
            for i in 0..m {
                result[i].assign(&result0[i + 1]);
            }
            return result;
        }
        Err(SolveErr::NotFeasible) => panic!("not feasible"),
        Err(SolveErr::MaximizableToInf) => panic!("maximizable to inf"),
    }

    /*
      free(result0);
      free(c0);
    */
}
