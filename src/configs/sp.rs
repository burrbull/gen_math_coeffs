use crate::{Config, Generator, PrecTarget, ToF64};
use core::f32;

impl PrecTarget for f32 {
    const PREC_TARGET: u32 = 24;
}

impl ToF64 for f32 {
    fn to_f64(self) -> f64 {
        self as f64
    }
}

impl Config<f32> for Generator<f32, crate::Sin> {
    const N: usize = 5;
    const S: usize = 81;
    const L: usize = 0;
    const P: Option<f32> = Some(0.37);
    const MIN: f32 = 0.;
    const MAX: f32 = f32::consts::FRAC_PI_2;
    const PMUL: f32 = 2.;
    const PADD: f32 = 1.;
    const FIXCOEF0: Option<f32> = Some(1.);
    const FIXCOEF1: Option<f32> = None;
    const FIXCOEF2: Option<f32> = None;
}

impl Config<f32> for Generator<f32, crate::Cos> {
    const N: usize = 5;
    const S: usize = 40;
    const L: usize = 0;
    const P: Option<f32> = None;
    const MIN: f32 = 0.;
    const MAX: f32 = f32::consts::FRAC_PI_2;
    const PMUL: f32 = 2.;
    const PADD: f32 = 2.;
    const FIXCOEF0: Option<f32> = Some(-0.5);
    const FIXCOEF1: Option<f32> = None;
    const FIXCOEF2: Option<f32> = None;
}

impl Config<f32> for Generator<f32, crate::SinPi4> {
    // xsincospi4
    const N: usize = 5;
    const S: usize = 30;
    const P: Option<f32> = Some(0.69);
    const L: usize = 2;
    const MIN: f32 = 0.;
    const MAX: f32 = 1.;
    const PMUL: f32 = 2.;
    const PADD: f32 = 1.;
    const FIXCOEF0: Option<f32> = None;
    const FIXCOEF1: Option<f32> = None;
    const FIXCOEF2: Option<f32> = None;
}

impl Config<f32> for Generator<f32, crate::CosPi4> {
    // xsincospi4
    const N: usize = 5;
    const S: usize = 60;
    const P: Option<f32> = Some(0.7);
    const L: usize = 1;
    const MIN: f32 = 0.;
    const MAX: f32 = 1.;
    const PMUL: f32 = 2.;
    const PADD: f32 = 2.;
    const FIXCOEF0: Option<f32> = None;
    const FIXCOEF1: Option<f32> = None;
    const FIXCOEF2: Option<f32> = None;
}

impl Config<f32> for Generator<f32, crate::Tan> {
    const N: usize = 7;
    const S: usize = 40;
    const L: usize = 2;
    const P: Option<f32> = None;
    const MIN: f32 = 0.;
    const MAX: f32 = f32::consts::FRAC_PI_4;
    const PMUL: f32 = 2.;
    const PADD: f32 = 1.;
    const FIXCOEF0: Option<f32> = Some(1.);
    const FIXCOEF1: Option<f32> = None;
    const FIXCOEF2: Option<f32> = None;
}

impl Config<f32> for Generator<f32, crate::Ln> {
    const N: usize = 5;
    const S: usize = 40;
    const L: usize = 2;
    const P: Option<f32> = None;
    const MIN: f32 = 1.; //0.75
    const MAX: f32 = 1.5;
    const PMUL: f32 = 2.;
    const PADD: f32 = 1.;
    const FIXCOEF0: Option<f32> = Some(2.);
    const FIXCOEF1: Option<f32> = None;
    const FIXCOEF2: Option<f32> = None;
}

impl Config<f32> for Generator<f32, crate::Exp> {
    const N: usize = 7;
    const S: usize = 50;
    const L: usize = 0;
    const P: Option<f32> = None;
    const MIN: f32 = -0.347;
    const MAX: f32 = 0.347; // 0.5 log 2
    const PMUL: f32 = 1.;
    const PADD: f32 = 0.;
    const FIXCOEF0: Option<f32> = Some(1.);
    const FIXCOEF1: Option<f32> = Some(1.);
    const FIXCOEF2: Option<f32> = None; // Some(0.5);
}

impl Config<f32> for Generator<f32, crate::Atan> {
    const N: usize = 10;
    const S: usize = 100;
    const L: usize = 2;
    const P: Option<f32> = None;
    const MIN: f32 = 0.;
    const MAX: f32 = 1.;
    const PMUL: f32 = 2.;
    const PADD: f32 = 1.;
    const FIXCOEF0: Option<f32> = Some(1.);
    const FIXCOEF1: Option<f32> = None;
    const FIXCOEF2: Option<f32> = None;
}
