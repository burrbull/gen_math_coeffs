use crate::{Config, Generator, PrecTarget, ToF64};
use core::f64;

impl PrecTarget for f64 {
    const PREC_TARGET: u32 = 53;
}

impl ToF64 for f64 {
    fn to_f64(self) -> f64 {
        self
    }
}

impl Config<f64> for Generator<f64, crate::Sin> {
    const N: usize = 8;
    const S: usize = 40;
    const L: usize = 4;
    const P: Option<f64> = None;
    const MIN: f64 = 0.;
    const MAX: f64 = f64::consts::FRAC_PI_4;
    const PMUL: f64 = 2.;
    const PADD: f64 = 1.;
    const FIXCOEF0: Option<f64> = Some(1.); // Fix coef 0 to 1.0
    const FIXCOEF1: Option<f64> = None;
    const FIXCOEF2: Option<f64> = None;
}

impl Config<f64> for Generator<f64, crate::Cos> {
    const N: usize = 10;
    const S: usize = 40;
    const L: usize = 2;
    const P: Option<f64> = None;
    const MIN: f64 = 0.;
    const MAX: f64 = f64::consts::FRAC_PI_4;

    const PMUL: f64 = 2.;
    const PADD: f64 = 2.;
    const FIXCOEF0: Option<f64> = Some(-0.5);
    const FIXCOEF1: Option<f64> = None;
    const FIXCOEF2: Option<f64> = None;
}

impl Config<f64> for Generator<f64, crate::SinPi4> {
    // for xsincospi4_u05
    const N: usize = 8;
    const S: usize = 40;
    const L: usize = 2;
    const P: Option<f64> = None;
    const MIN: f64 = 0.;
    const MAX: f64 = 1.;
    const PMUL: f64 = 2.;
    const PADD: f64 = 1.;
    const FIXCOEF0: Option<f64> = None;
    const FIXCOEF1: Option<f64> = None;
    const FIXCOEF2: Option<f64> = None;
}
/*
impl Config<f64> for Generator<f64, crate::SinPi4> {
    // for xsincospi4
    const N: usize = 7;
    const S: usize = 40;
    const L: usize = 0;
    const P: Option<f64> = None;
    const MIN: f64 = 0.;
    const MAX: f64 = 1.;
    const PMUL: f64 = 2.;
    const PADD: f64 = 1.;
    const FIXCOEF0: Option<f64> = None;
    const FIXCOEF1: Option<f64> = None;
    const FIXCOEF2: Option<f64> = None;
}
*/
impl Config<f64> for Generator<f64, crate::CosPi4> {
    // for xsincospi4_u05
    const N: usize = 8;
    const S: usize = 40;
    const L: usize = 2;
    const P: Option<f64> = None;
    const MIN: f64 = 0.;
    const MAX: f64 = 1.;
    const PMUL: f64 = 2.;
    const PADD: f64 = 2.;
    const FIXCOEF0: Option<f64> = None;
    const FIXCOEF1: Option<f64> = None;
    const FIXCOEF2: Option<f64> = None;
}

impl Config<f64> for Generator<f64, crate::Tan> {
    const N: usize = 17;
    const S: usize = 60;
    const L: usize = 0;
    const P: Option<f64> = None;
    const MIN: f64 = 0.;
    const MAX: f64 = f64::consts::FRAC_PI_4;
    const PMUL: f64 = 2.;
    const PADD: f64 = 1.;
    const FIXCOEF0: Option<f64> = Some(1.);
    const FIXCOEF1: Option<f64> = None;
    const FIXCOEF2: Option<f64> = None;
}

impl Config<f64> for Generator<f64, crate::Ln> {
    const N: usize = 11;
    const S: usize = 35;
    const L: usize = 2;
    const P: Option<f64> = None;
    const MIN: f64 = 1.; //0.75
    const MAX: f64 = 1.5;
    const PMUL: f64 = 2.;
    const PADD: f64 = 1.;
    const FIXCOEF0: Option<f64> = Some(2.);
    const FIXCOEF1: Option<f64> = None;
    const FIXCOEF2: Option<f64> = None;
}

impl Config<f64> for Generator<f64, crate::Exp> {
    const N: usize = 12;
    const S: usize = 50;
    const L: usize = 2;
    const P: Option<f64> = None;
    const MIN: f64 = -0.347;
    const MAX: f64 = 0.347; // 0.5 log 2
    const PMUL: f64 = 1.;
    const PADD: f64 = 0.;
    const FIXCOEF0: Option<f64> = Some(1.);
    const FIXCOEF1: Option<f64> = Some(1.);
    const FIXCOEF2: Option<f64> = None; // Some(0.5)
}

impl Config<f64> for Generator<f64, crate::Atan> {
    const N: usize = 21;
    const S: usize = 100;
    const L: usize = 1;
    const P: Option<f64> = Some(1.1);
    const MIN: f64 = 0.;
    const MAX: f64 = 1.;
    const PMUL: f64 = 2.;
    const PADD: f64 = 1.;
    const FIXCOEF0: Option<f64> = Some(1.);
    const FIXCOEF1: Option<f64> = None;
    const FIXCOEF2: Option<f64> = None;
}

impl Config<f64> for Generator<f64, crate::Asin> {
    const N: usize = 20;
    const S: usize = 100;
    const L: usize = 0;
    const P: Option<f64> = Some(1.54);
    const MIN: f64 = 0.;
    const MAX: f64 = 0.708;
    const PMUL: f64 = 2.;
    const PADD: f64 = 1.;
    const FIXCOEF0: Option<f64> = Some(1.);
    const FIXCOEF1: Option<f64> = None;
    const FIXCOEF2: Option<f64> = None;
}
