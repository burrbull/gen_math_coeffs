use crate::{Config, Generator, PrecTarget, ToF64};
use softposit::{MathConsts, P32};

impl PrecTarget for P32 {
    const PREC_TARGET: u32 = 27;
}

impl ToF64 for P32 {
    fn to_f64(self) -> f64 {
        self.into()
    }
}

const TWO: P32 = P32::new(0x_4800_0000);

impl Config<P32> for Generator<P32, crate::Sin> {
    const N: usize = 6;
    const S: usize = 70;
    const L: usize = 1;
    const P: Option<P32> = Some(P32::new(0x_33d7_0a3d)); // 0.37
    const MIN: P32 = P32::ZERO;
    const MAX: P32 = P32::FRAC_PI_2; //pi/2
    const PMUL: P32 = TWO;
    const PADD: P32 = P32::ONE;
    const FIXCOEF0: Option<P32> = Some(P32::ONE); // Fix coef 0 to 1.0
    const FIXCOEF1: Option<P32> = None;
    const FIXCOEF2: Option<P32> = None;
}

impl Config<P32> for Generator<P32, crate::Cos> {
    const N: usize = 6;
    const S: usize = 40;
    const L: usize = 0;
    const P: Option<P32> = None;
    const MIN: P32 = P32::ZERO;
    const MAX: P32 = P32::FRAC_PI_2;
    const PMUL: P32 = TWO;
    const PADD: P32 = TWO;
    const FIXCOEF0: Option<P32> = Some(P32::new(-0x_3800_0000));
    const FIXCOEF1: Option<P32> = None;
    const FIXCOEF2: Option<P32> = None;
}

impl Config<P32> for Generator<P32, crate::Tan> {
    const N: usize = 8;
    const S: usize = 40;
    const L: usize = 2;
    const P: Option<P32> = None;
    const MIN: P32 = P32::ZERO;
    const MAX: P32 = P32::FRAC_PI_4;
    const PMUL: P32 = TWO;
    const PADD: P32 = P32::ONE;
    const FIXCOEF0: Option<P32> = Some(P32::ONE);
    const FIXCOEF1: Option<P32> = None;
    const FIXCOEF2: Option<P32> = None;
}

impl Config<P32> for Generator<P32, crate::Atan> {
    const N: usize = 10;
    const S: usize = 100;
    const L: usize = 2;
    const P: Option<P32> = None;
    const MIN: P32 = P32::ZERO;
    const MAX: P32 = P32::ONE;
    const PMUL: P32 = TWO;
    const PADD: P32 = P32::ONE;
    const FIXCOEF0: Option<P32> = Some(P32::ONE);
    const FIXCOEF1: Option<P32> = None;
    const FIXCOEF2: Option<P32> = None;
}

impl Config<P32> for Generator<P32, crate::Asin> {
    const N: usize = 7;
    const S: usize = 100;
    const L: usize = 0;
    const P: Option<P32> = Some(P32::new(0x_4451_eb85));
    const MIN: P32 = P32::ZERO;
    const MAX: P32 = P32::new(0x_3800_0000 /*0x_3b53_f7cf*/);
    const PMUL: P32 = TWO;
    const PADD: P32 = P32::ONE;
    const FIXCOEF0: Option<P32> = Some(P32::ONE);
    const FIXCOEF1: Option<P32> = None;
    const FIXCOEF2: Option<P32> = None;
}

impl Config<P32> for Generator<P32, crate::Exp> {
    const N: usize = 8;
    const S: usize = 50;
    const L: usize = 0;
    const P: Option<P32> = None;
    const MIN: P32 = P32::new(-0x_331a_9fbe); //0.347;
    const MAX: P32 = P32::new(0x_331a_9fbe); // 0.5 log 2
    const PMUL: P32 = P32::ONE;
    const PADD: P32 = P32::ZERO;
    const FIXCOEF0: Option<P32> = Some(P32::ONE);
    const FIXCOEF1: Option<P32> = Some(P32::ONE);
    const FIXCOEF2: Option<P32> = None; // Some(0.5);
}

impl Config<P32> for Generator<P32, crate::Ln> {
    const N: usize = 6;
    const S: usize = 40;
    const L: usize = 2;
    const P: Option<P32> = None;
    const MIN: P32 = P32::ONE; //0.75
    const MAX: P32 = P32::new(0x_4400_0000); // 1.5;
    const PMUL: P32 = TWO;
    const PADD: P32 = P32::ONE;
    const FIXCOEF0: Option<P32> = Some(TWO);
    const FIXCOEF1: Option<P32> = None;
    const FIXCOEF2: Option<P32> = None;
}
